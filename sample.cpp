///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 10x - Collection Class Evaluator
///
/// @file sample.cpp
/// @version 1.0
///
/// Sample code I used for my testing
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 10x - Collection Class Evaluator - EE 205 - Spr 2021
/// @date   14_MAY_2021
///////////////////////////////////////////////////////////////////////////////


/// This abstract test class is a template for concrete classes that can
/// actually do tests.
class ABSTRACT_TEST_CLASS {
protected:
	static long start, end;
	static int  insert_value;
	static int  search_value;

public:
	virtual inline void    initDataStructure()  __attribute__((always_inline)) = 0;
	virtual inline ticks_t testInsert()         __attribute__((always_inline)) = 0;
	virtual inline ticks_t clearDataStructure() __attribute__((always_inline)) = 0;
	virtual inline ticks_t testSearch()         __attribute__((always_inline)) = 0;
};

long ABSTRACT_TEST_CLASS::start;
long ABSTRACT_TEST_CLASS::end;
int  ABSTRACT_TEST_CLASS::insert_value;
int  ABSTRACT_TEST_CLASS::search_value;




//////////////////////////////////  Vector  //////////////////////////////////
///
/// This class wraps a vector<long> container and has methods to:
///   - Initialize the vector
///   - Insert data into the vector (measuring the insertion time)
///   - Clear data out of the vector
///   - Search for data in the vector
class TestVector : public ABSTRACT_TEST_CLASS {
private:
	static vector<long> container;

public:
	virtual inline void initDataStructure() __attribute__((always_inline)) { container.clear(); }

	virtual inline ticks_t testInsert() __attribute__((always_inline)) {
		auto endIterator = container.end();
		insert_value = rand();

		MARK_TIME( start );

		// Do the operation ///////
		container.insert( endIterator, insert_value );
		///////////////////////////

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t clearDataStructure()   __attribute__((always_inline)) {
		MARK_TIME( start );

		container.clear();

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t testSearch()   __attribute__((always_inline)) {
		search_value = rand();

		MARK_TIME( start );

		for( auto i : container ) {
			if( i == search_value )
				break;
		}

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

}; // Vector

vector<long> TestVector::container;




//////////////////////////////////  Result  //////////////////////////////////
class Result {
private:
	ABSTRACT_TEST_CLASS* pTestClass = nullptr;

	string structure = "";
	ticks_t insert_ticks[TESTS];                          // Sum of all ticks
	ticks_t clear_ticks[TESTS];                          // Sum of all ticks
	ticks_t search_ticks[TESTS];   // Sum of search ticks

public:
	Result( ABSTRACT_TEST_CLASS* pNewTestClass, string_view newStructureName ) {
		clear();
		pTestClass = pNewTestClass;
		structure  = newStructureName;
	}

	const string_view getStructure() const { return structure; }

	void clear() {
		structure = "";
		memset( insert_ticks, 0, sizeof( insert_ticks) );
		memset( clear_ticks,  0, sizeof( clear_ticks) );
		memset( search_ticks, 0, sizeof( search_ticks) );
	}

	void recordInsertTicks( size_t testIndex, size_t runIndex, ticks_t result ) {
		if( runIndex >= THROWAWAY_RUNS ) {
			insert_ticks[testIndex]  += result;
		}
	}

	void recordClearTicks( size_t testIndex, size_t runIndex, ticks_t result ) {
		if( runIndex >= THROWAWAY_RUNS ) {
			clear_ticks[testIndex]  += result;
		}
	}

	void recordSearchTicks( size_t testIndex, size_t runIndex, ticks_t result ) {
		if( runIndex >= THROWAWAY_RUNS ) {
			search_ticks[testIndex]  += result;
		}
	}

	static void printResultsHeader() {
		printf( "%21s", " " );
		for( unsigned int test = 0 ; test < TESTS ; test++ ) {
			printf( "%13u ", TEST_SIZE[test] );
		}
		printf( "\n" );

		printf( "==================== " );
		for( unsigned int test = 0 ; test < TESTS ; test++ ) {
			printf( "============= " );
		}
		printf( "\n" );
	}

	void printResults() const {
		printf( "%-20s", (structure + " insert").c_str() );

		for( unsigned int test = 0 ; test < TESTS ; test++ ) {
			ticks_t average = insert_ticks[test] / ( TEST_RUNS * TEST_SIZE[test] );
			printf( "%14lu", average );
		}
		printf( "\n" );

		printf( "%-20s", (structure + " search").c_str() );

		for( unsigned int test = 0 ; test < TESTS ; test++ ) {
			ticks_t average = search_ticks[test] / ( TEST_RUNS * SEARCH_TESTS );
			printf( "%14lu", average );
		}
		printf( "\n" );


		printf( "%-20s", (structure + " clear").c_str() );

		for( unsigned int test = 0 ; test < TESTS ; test++ ) {
			ticks_t average = clear_ticks[test] / ( TEST_RUNS );
			printf( "%14lu", average );
		}
		printf( "\n" );

		printf( "- - - - - - - - - - \n" );
	}

	void testDataStructure() {
		for( size_t testIndex = 0 ; testIndex < TESTS ; testIndex++ ) {
			unsigned int testSize = TEST_SIZE[testIndex];  // Store locally, so we aren't always computing it

			for( size_t run = 0 ; run < THROWAWAY_RUNS + TEST_RUNS ; run++ ) {
				// Setup the test infrastructure
				ticks_t accumulator = 0;

				// Setup the test //////
				////////////////////////
				pTestClass->initDataStructure();

				// Insert the data
				for( size_t loop = 0 ; loop < testSize ; loop++ ) {
					accumulator += pTestClass->testInsert();
				}

				// Record the inserts
				recordInsertTicks( testIndex, run, accumulator );

				// Search the data
				accumulator = 0;
				for( size_t loop = 0 ; loop < SEARCH_TESTS ; loop++ ) {
					accumulator += pTestClass->testSearch();
				}
				recordSearchTicks( testIndex, run, accumulator );

				// Clear the data structure and record the results
				recordClearTicks( testIndex, run, pTestClass->clearDataStructure() );

#ifdef PRINT_PROGRESS
				// Print progress
				cout << ".";
				cout.flush();
#endif
			} // testIndex : TESTS

#ifdef PRINT_PROGRESS
		// Print progress
		cout << " >> ";
		cout.flush();
#endif

		} // run : RUNS
	} // testDataStructure()

} ; // Result



Result::printResultsHeader();

TestVector testVector;
Result vectorResult( &testVector, "vector" );
vectorResult.testDataStructure();
vectorResult.printResults();
