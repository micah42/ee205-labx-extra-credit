###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 10x - Collection Class Evaluator
#
# @file    Makefile
# @version 1.0
#
# @author Micah Chinen <micah42@hawaii.edu>
# @brief  Lab 10x - Collection Class Evaluator - EE 205 - Spr 2021
# @date   14_MAY_2021
###############################################################################

CXX      = g++
CXXFLAGS = -std=c++20    \
           -Wall         \
           -pedantic     \
           -Wshadow      \
           -Wconversion

TARGETS  = eval

all: $(TARGETS)

$(TARGETS): %: %.cpp
#	$(CXX) $(CXXFLAGS) -S -fverbose-asm $^
	$(CXX) $(CXXFLAGS) -o $@ $^

clean:
	rm -f *.o *.a $(TARGETS)
